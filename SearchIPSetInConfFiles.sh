#!/bin/bash
RUTA=`pwd`
###NODOS="nodos_revisa.txt"
NODOS=$1
LOGS="${RUTA}/log"
CMD_SSH="ssh -o PasswordAuthentication=no -o StrictHostKeyChecking=no -o ConnectTimeout=5 -o BatchMode=yes -q"
CMD_SCP="scp -o ConnectTimeout=10"
cuenta=`cat $RUTA/$NODOS|wc -l`
echo $cuenta
> ${LOGS}/sal_busca.dat
cuenta=$(( cuenta - 0 ))
   while [ $cuenta -ge  1 ]
        do
           nodo=`cat $RUTA/$NODOS | tail -$cuenta |head -1`
           $CMD_SSH $nodo 'find /u01/home/  -type f ' > ${LOGS}/lista_paso.tmp
           cat ${LOGS}/lista_paso.tmp |grep -v .ssh |grep -v oper |grep -v sweb |grep -v adms |grep -v sist |grep -v ctm |grep -v ingsis > ${LOGS}/lista.tmp
 
           lineas=`cat  ${LOGS}/lista.tmp |wc -l`
           lineas=$(( lineas - 0 ))
           echo $lineas
                while [ $lineas -ge 1 ]
                      do
                         linea=`cat ${LOGS}/lista.tmp |tail -$lineas |head -1`
                        ###$CMD_SSH $nodo 'cat '${linea}' |grep -oi "[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}"'
                         IP=`$CMD_SSH $nodo 'cat '${linea}' | grep -oi "[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}" |grep -v 127.0.0.1 |grep -v known_hosts'`
                         if [ "${IP}" != "" ]
                           then
                               echo "${nodo} IP en ${linea}">>${LOGS}/sal_busca.dat
                               ##echo "IP en ${linea}">>${LOGS}/sal_busca.dat
                               echo ${IP} >>${LOGS}/sal_busca.dat
                         fi
                              lineas=$(( lineas - 1 ))
                      done
 
          cuenta=$(( cuenta - 1 ))
    done
    cat ${LOGS}/sal_busca.dat
 
