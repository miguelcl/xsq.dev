#!/bin/bash

IPT="/sbin/iptables"

printf "PATH IPTABLES= $IPT"
IFACE_LAN=$( ip route  | grep default | awk '{ printf $5}' )
IP_LAN=$(ip address | grep inet | grep $IFACE_LAN | awk '{ printf $2}'| cut -d"/" -f1)
ALL="0.0.0.0/0"
LOC="127.0.0.1"
printf "\nIP LAN\t:\t $IP_LAN"
printf "\nIFACE LAN\t:\t $IFACE_LAN\n\n"

$IPT -F
$IPT -F -t nat
$IPT -F -t mangle

$IPT -X
$IPT -X -t nat
$IPT -X -t mangle

$IPT -Z
$IPT -Z -t nat
$IPT -Z -t mangle
printf "\nCLEAN RULES OK"

$IPT -P INPUT   ACCEPT
$IPT -P FORWARD ACCEPT
$IPT -P OUTPUT  ACCEPT

printf "\nDEFAULT RULES SET IN ACCEPT TRAFFIC MODE"

$IPT -A INPUT  -i $IFACE_LAN  -m state --state NEW -j LOG  --log-prefix "FIREWALL-IN:"  
#$IPT -A INPUT  -i $IFACE_LAN  -m state --state NEW -j LOG  --log-prefix "FIREWALL-IN:" 
#$IPT -A INPUT  -i $IFACE_LAN  -m state --state NEW -j LOG  --log-prefix "FIREWALL-IN:" 
#$IPT -A INPUT  -i $IFACE_LAN  -m state --state NEW -j LOG  --log-prefix "FIREWALL-IN:" 
#$IPT -A INPUT  -i $IFACE_LAN  -m state --state NEW -j LOG  --log-prefix "FIREWALL-IN:" 
#$IPT -A INPUT  -i $IFACE_LAN  -m state --state NEW -j LOG  --log-prefix "FIREWALL-IN:" 

$IPT -A OUTPUT -o $IFACE_LAN -p tcp   -m state --state NEW -j LOG  --log-prefix "FIREWALL-OUT:"  --log-level 5
$IPT -A OUTPUT -o $IFACE_LAN -p icmp  -m state --state NEW -j LOG  --log-prefix "FIREWALL-OUT:"  --log-level 5
# EXCLUDE UDP RANGE PORT DNS,SYSLOG 
$IPT -A OUTPUT -o $IFACE_LAN -p udp --dport 10:52      -m state --state NEW -j LOG  --log-prefix "FIREWALL-OUT:"  --log-level 5
$IPT -A OUTPUT -o $IFACE_LAN -p udp --dport 54:513     -m state --state NEW -j LOG  --log-prefix "FIREWALL-OUT:"  --log-level 5
$IPT -A OUTPUT -o $IFACE_LAN -p udp --dport 515:65335  -m state --state NEW -j LOG  --log-prefix "FIREWALL-OUT:"  --log-level 5
#$IPT -A OUTPUT -o $IFACE_LAN  -m state --state NEW -j LOG  --log-prefix "FIREWALL-OUT:" 
#$IPT -A OUTPUT -o $IFACE_LAN  -m state --state NEW -j LOG  --log-prefix "FIREWALL-OUT:" 
#$IPT -A OUTPUT -o $IFACE_LAN  -m state --state NEW -j LOG  --log-prefix "FIREWALL-OUT:" 
#$IPT -A OUTPUT -o $IFACE_LAN  -m state --state NEW -j LOG  --log-prefix "FIREWALL-OUT:" 


printf "\nRULES LOG ALL TRAFFIC ARE OK  \n"

# SAVE FW CONF:
service iptables save

#####################################################################################
#
# Level  name               Description
# 0      emerg or panic     Something is incredibly wrong; the system is probably about to crash
# 1      alert              Immediate attention is required
# 2      crit               Critical hardware or software failure
# 3      error              Usually used for reporting of hardware problems by drivers  
# 4      warning            Something isn't right, but the problem is not serious
# 5      notice             No problems; indicates an advisory of some sort.
# 6      info               General information
# 7      debug              Deguging
